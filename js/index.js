// Script para tooltip
var tooltipTriggerList = [].slice.call(
  document.querySelectorAll('[data-bs-toggle="tooltip"]')
);
var tooltipList = tooltipTriggerList.map(function(tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl);
});

// Script para popover
var popoverTriggerList = [].slice.call(
  document.querySelectorAll('[data-bs-toggle="popover"]')
);
var popoverList = popoverTriggerList.map(function(popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl);
});

// Script que modifica la velocidad del carousel
var myCarousel = document.querySelector("#carouselControls");
var carousel = new bootstrap.Carousel(myCarousel, {
  interval: 2000,
});

// JQuery
$(function() {
  const Btn_num = ["0", "1", "2", "3", "4", "5"];
  $("#contacto").on("show.bs.modal", function(e) {
    console.log("El modal se está mostrando");
    Btn_num.forEach(function(element) {
      $("#contactoBtn" + Btn_num[element]).removeClass("btn-outline-success");
    });
    Btn_num.forEach(function(element) {
      $("#contactoBtn" + Btn_num[element]).addClass("btn-primary");
    });
    Btn_num.forEach(function(element) {
      $("#contactoBtn" + Btn_num[element]).prop("disabled", true);
    });
  });

  $("#contacto").on("shown.bs.modal", function(e) {
    console.log("El modal se mostró");
  });
  $("#contacto").on("hide.bs.modal", function(e) {
    console.log("El modal se oculta");
  });

  $("#contacto").on("hidden.bs.modal", function(e) {
    console.log("El modal se ocultó");
    Btn_num.forEach(function(element) {
      $("#contactoBtn" + Btn_num[element]).removeClass("btn-primary");
    });
    Btn_num.forEach(function(element) {
      $("#contactoBtn" + Btn_num[element]).addClass("btn-outline-success");
    });
    Btn_num.forEach(function(element) {
      $("#contactoBtn" + Btn_num[element]).prop("disabled", false);
    });
  });
});

// Scripts formas alternativas
//   $(function(){
//     $("[data-bs-toggle='tooltip']").tooltip();
//     $("[data-bs-toggle='popover']").popover();

//     $('.carousel').carousel({
//       interval: 2000
//     });

//     $('#contacto').on('show.bs.modal', function (e){
//       console.log('El modal se está mostrando');
//     });
//   })
